import '../../core/result/app_result.dart';
import '../../data/response/base_response.dart';
import '../entities/home_entity.dart';

abstract class HomeRepository{
  Future<AppResult<AppFeedResponse<List<HomeEntity>>>> getListPokemon(int offset);
}