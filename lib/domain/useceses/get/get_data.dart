import '../../../core/result/app_result.dart';
import '../../../core/usecases/usecase.dart';
import '../../../data/response/base_response.dart';
import '../../entities/home_entity.dart';
import '../../repositories/home_repository.dart';

class GetData implements UseCase<AppResult<AppFeedResponse<List<HomeEntity>>>, int>{
  final HomeRepository homeRepository;

  GetData({required this.homeRepository});

  @override
  Future<AppResult<AppFeedResponse<List<HomeEntity>>>> call(int params) {
    return homeRepository.getListPokemon(params);
  }
}