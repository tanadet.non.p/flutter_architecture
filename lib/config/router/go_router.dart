import 'package:flutter/material.dart';
import 'package:flutter_structure/data/arguments/test1_argument.dart';
import 'package:flutter_structure/main.dart';
import 'package:flutter_structure/presenter/views/home/home_view.dart';
import 'package:flutter_structure/presenter/views/test1/test1_view.dart';
import 'package:go_router/go_router.dart';

import 'go_path.dart';

class AppRoute{
  static GoRouter router = GoRouter(
    routes: <GoRoute>[
      GoRoute(
        routes: <GoRoute>[
          GoRoute(
            path: GoPath.home,
            name: GoPath.home,
            builder: (BuildContext context, GoRouterState state) => HomeView(),
            routes: [
              GoRoute(
                path: GoPath.test1,
                name: GoPath.test1,
                builder: (BuildContext context, GoRouterState state) {
                  final data = state.extra as Test1Argument;
                  return Test1View(data: data.data, text: data.dataString,);
                },
              ),
            ]
          ),
        ],
        path: '/',
        builder: (BuildContext context, GoRouterState state) => const MyHomePage(title: ''),
      ),
    ],
    errorBuilder: (context, state) => const Scaffold(body: Center(child: Text('Error View'),)),
  );
}

