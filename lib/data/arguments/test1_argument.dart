import '../../domain/entities/home_entity.dart';

class Test1Argument{
  final HomeEntity data;
  final String dataString;

  Test1Argument({required this.data, required this.dataString});
}