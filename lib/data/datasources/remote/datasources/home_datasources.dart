import 'package:flutter_structure/data/models/home_model.dart';

import '../../../../core/result/app_result.dart';
import '../../../response/base_response.dart';

abstract class HomeDatasource{
  Future<AppResult<AppFeedResponse<List<HomeModel>>>> getListPokemon(int offset);
}