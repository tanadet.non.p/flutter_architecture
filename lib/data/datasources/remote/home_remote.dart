import 'package:dio/dio.dart';
import 'package:flutter_structure/core/result/app_result.dart';
import 'package:flutter_structure/data/datasources/remote/datasources/home_datasources.dart';
import 'package:flutter_structure/data/models/home_model.dart';
import 'package:flutter_structure/data/response/base_response.dart';

import '../../response/home_response.dart';

class HomeRemote implements HomeDatasource{

  final Dio dio;

  HomeRemote({required this.dio});

  final String _getListPokemon = 'pokemon?limit=12';

  @override
  Future<AppResult<AppFeedResponse<List<HomeModel>>>> getListPokemon(int offset) async {
    try{
      Response dioResponse = await dio.get('$_getListPokemon&offset=$offset');

      HomeResponse response = HomeResponse.fromJson(dioResponse.data);

      List<HomeModel> listResult = [];

      for(ItemResponse itemResponse in response.listData){
        listResult.add(HomeModel.fromResponse(itemResponse));
      }

      return Success(value: AppFeedResponse(countPage: response.countPage, lastPage: response.lastPage, numPage: response.offset, valueFeed: listResult));
    }on DioException catch(e){
      return Error(dioException: e, errorObject: e.response?.data);
    }
  }
}