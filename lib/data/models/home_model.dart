import 'package:flutter_structure/domain/entities/home_entity.dart';

import '../response/home_response.dart';

class HomeModel extends HomeEntity{
  HomeModel({
    required super.id,
    required super.name,
    required super.imageUrl,
    required super.url
  });

  factory HomeModel.fromResponse(ItemResponse itemResponse)=> HomeModel(
      id: itemResponse.id,
      name: itemResponse.name,
      imageUrl: itemResponse.imageUrl,
      url: itemResponse.url
  );
}