import 'package:flutter_structure/core/result/app_result.dart';
import 'package:flutter_structure/data/datasources/remote/datasources/home_datasources.dart';
import 'package:flutter_structure/data/response/base_response.dart';
import 'package:flutter_structure/domain/entities/home_entity.dart';
import 'package:flutter_structure/domain/repositories/home_repository.dart';

class HomeRepositoryImpl implements HomeRepository{
  final HomeDatasource homeDatasource;

  HomeRepositoryImpl({required this.homeDatasource});

  @override
  Future<AppResult<AppFeedResponse<List<HomeEntity>>>> getListPokemon(int offset) {
    return homeDatasource.getListPokemon(offset);
  }
}