
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_structure/presenter/views/home/state/home_state.dart';

import '../../../config/network/app_dio_option.dart';
import '../../../data/datasources/remote/datasources/home_datasources.dart';
import '../../../data/datasources/remote/home_remote.dart';
import '../../../data/repositories/home_repository_impl.dart';
import '../../../domain/repositories/home_repository.dart';
import '../../../domain/useceses/get/get_data.dart';
import 'home_controller.dart';

final homeRemoteDataSource = Provider<HomeDatasource>((ref) => HomeRemote(dio: ref.read(getDioOption).initDioOption()));

final homeRepositoryProvider = Provider<HomeRepository>((ref) => HomeRepositoryImpl(
    homeDatasource: ref.read(homeRemoteDataSource)
));

final getData = Provider((ref) => GetData(homeRepository: ref.read(homeRepositoryProvider)));

final homePageStateProvider = StateNotifierProvider<HomeController, HomeState>(
      (ref) => HomeController(
    getData: ref.read(getData),
  ),
);