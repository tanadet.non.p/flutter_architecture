import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_structure/data/models/home_model.dart';
import 'package:flutter_structure/domain/useceses/get/get_data.dart';
import 'package:flutter_structure/presenter/views/home/state/home_state.dart';

import '../../../core/result/app_result.dart';
import '../../../data/models/app_feed.dart';
import '../../../data/response/base_response.dart';
import '../../../domain/entities/home_entity.dart';

class HomeController extends StateNotifier<HomeState>{
  final GetData _getData;

  HomeController({required GetData getData}) :
        _getData = getData,
        super(const HomeState());

  AppFeed<HomeEntity> _appFeedPokemonObs = AppFeed(feed: []);

  Future<void> getPokemon({forceReload = true}) async {
    AppFeed<HomeEntity> appFeed = _appFeedPokemonObs;

    if(appFeed.feed.isEmpty || forceReload){
      state = state.copyWith(status: AppHomePageStatus.loading);
      appFeed = AppFeed(
          feed: [],
          numPage: 0,
          lastPage: false,
          countPage: 0
      );
    }

    AppResult<AppFeedResponse<List<HomeEntity>>> appResult = await _getData.call(appFeed.numPage);

    appResult.whenWithResult((result) {
      appFeed.numPage = result.value.numPage;
      appFeed.lastPage = result.value.lastPage;
      appFeed.feed = result.value.valueFeed;
      appFeed.countPage = result.value.countPage;

      _appFeedPokemonObs = appFeed;

      state = state.copyWith(status: AppHomePageStatus.success, value: _appFeedPokemonObs);
    }, (error) {
      state = state.copyWith(status: AppHomePageStatus.failure);
    });
  }

  void addItem(){
    print(_appFeedPokemonObs);
    _appFeedPokemonObs.feed.add(HomeModel(id: 013, name: 'test', imageUrl: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${13}.png', url: ''));
    state = state.copyWith(value: _appFeedPokemonObs);
  }
}