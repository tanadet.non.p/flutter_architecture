import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_structure/presenter/widgets/home/list_item_widget.dart';

import 'home_provider.dart';

class HomeView extends ConsumerStatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  ConsumerState<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends ConsumerState<HomeView> {

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      ref.read(homePageStateProvider.notifier).getPokemon();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 16.w, top: 16.h, right: 16.w),
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset('assets/images/pokeball.png',
                        height: 24.h,
                        width: 24.w,
                        color: Colors.white,
                      ),
                      SizedBox(width: 16.w,),
                      Text(
                        'Pokédex',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24.sp,
                          fontWeight: FontWeight.w700,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 8.h,),
                  Row(
                    children: [
                      Expanded(
                        child: TextField(
                          // controller: _searchController,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(vertical: 10),
                            filled: true,
                            fillColor: Colors.white,
                            prefixIconColor: Colors.blue,
                            hintText: 'Search',
                            prefixIcon: const Icon(Icons.search,),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16.0),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 16.w,),
                      GestureDetector(
                        onTap: ()=> ref.read(homePageStateProvider.notifier).addItem(),
                        child: Container(
                          padding: const EdgeInsets.all(10),
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            // borderRadius: BorderRadius.circular(70),
                            color: Colors.white,
                          ),
                          child: Image.asset('assets/images/text_format.png',
                            height: 16.h,
                            width: 16.w,
                            color: Colors.blue,
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 20.h,),
                ],),
            ),
            const ListItemWidget()
          ],
        ),
      ),
    );
  }
}