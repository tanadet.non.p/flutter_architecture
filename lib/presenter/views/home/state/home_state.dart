import 'package:equatable/equatable.dart';

import '../../../../data/models/app_feed.dart';
import '../../../../domain/entities/home_entity.dart';

enum AppHomePageStatus { initial, loading, success, failure }

class HomeState extends Equatable {
  final AppHomePageStatus status;
  final AppFeed<HomeEntity>? value;

  const HomeState({
    this.status = AppHomePageStatus.initial,
    this.value
  });

  HomeState copyWith({
    AppHomePageStatus? status,
    AppFeed<HomeEntity>? value
  }) {
    return HomeState(
        status: status ?? this.status,
        value: value ?? this.value
    );
  }

  @override
  List<Object> get props => [
    status,
    value!
  ];
}