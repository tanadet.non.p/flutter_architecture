import 'package:flutter/material.dart';
import 'package:flutter_structure/domain/entities/home_entity.dart';
import 'package:go_router/go_router.dart';

class Test1View extends StatefulWidget {
  final HomeEntity data;
  final String text;
  const Test1View({Key? key, required this.data, required this.text}) : super(key: key);

  @override
  State<Test1View> createState() => _Test1ViewState();
}

class _Test1ViewState extends State<Test1View> {

  @override
  void initState() {
    print('testData Form Argument =====> ${widget.data.name}');
    print('text ====> ${widget.text}');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('test1'),
        leading: IconButton(
          onPressed: () {
            context.pop('test');
          },
          icon: const Icon(Icons.arrow_back_ios),
        ),
      ),
      body: const Center(
        child: Text('test1 view'),
      ),
    );
  }
}
