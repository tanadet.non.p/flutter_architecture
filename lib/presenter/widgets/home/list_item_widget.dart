import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_structure/presenter/widgets/home/pokemon_card.dart';

import '../../../data/models/app_feed.dart';
import '../../../domain/entities/home_entity.dart';
import '../../views/home/home_provider.dart';
import '../../views/home/state/home_state.dart';

class ListItemWidget extends ConsumerWidget {
  const ListItemWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final dataObs = ref.watch(homePageStateProvider);

    Widget widgetBuilder = Container();

    switch(dataObs.status){
      case AppHomePageStatus.initial :
      case AppHomePageStatus.loading : widgetBuilder = const Center(child: CircularProgressIndicator());
      case AppHomePageStatus.success : {
        final data = dataObs.value as AppFeed<HomeEntity>;

        widgetBuilder = Expanded(
          child: Container(
            margin: const EdgeInsets.all(4.0),
            // padding: const EdgeInsets.all(4.0),
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.white
            ),
            child: GridView.builder(
              // physics: const NeverScrollableScrollPhysics(),
              itemCount: data.feed.length,
              shrinkWrap: true,
              padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 24.h),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisSpacing: 8,
                crossAxisSpacing: 8,
              ),
              itemBuilder: (BuildContext context, int index) {
                return ItemCard(data: data.feed[index]);
              },
            ),
          ),
        );
      }
      case AppHomePageStatus.failure : widgetBuilder = const Center(child: Text('Error'));
    }

    return widgetBuilder;
  }
}
